import {
  controls
} from '../../constants/controls';



export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    const initHealth1 = firstFighter.health;
    const initHealth2 = secondFighter.health;
    const keys = {};
    const firstAttacker = 'firstFighter';
    const secondAttacker = 'secondFighter';
    const firstAttackerImgbox = document.querySelector('.arena___left-fighter');
    const secondAttackerImgbox = document.querySelector('.arena___right-fighter');
    const firstFighterBar = document.getElementById('left-fighter-indicator');
    const secondFighterBar = document.getElementById('right-fighter-indicator');
    const arena = document.querySelector('.arena___battlefield');

    let winner;
    let blockFighterOne = false;
    let blockFighterTwo = false;

    window.addEventListener("keydown", keysPressed, false);
    window.addEventListener("keyup", keysReleased, false);

    const damageColor = "rgba(158, 9, 9, 0.6)";
    const blockColor = "rgba(15, 158, 22, 0.6)";

    function keysPressed(e) {
      keys[e.code] = true;
      if (firstFighter.health > 0 && secondFighter.health > 0) {

        if (controls.PlayerOneCriticalHitCombination.every((key, index) => {
            return keys[key];
          }) && !blockFighterOne) {
          console.log(`${firstFighter.name} is SUPER attacking`);
          updateHealth(firstFighter, secondFighter, secondFighterBar, superAttack, initHealth2)
          delay(firstAttacker);
          move(firstAttacker, damageColor);
        } else if (controls.PlayerTwoCriticalHitCombination.every((key, index) => {
            return keys[key];
          }) && !blockFighterTwo) {
          console.log(`${secondFighter.name} is SUPER attacking`);
          updateHealth(secondFighter, firstFighter, firstFighterBar, superAttack, initHealth1);
          delay(secondAttacker);
          move(secondAttacker, damageColor);
        } else if (keys[controls.PlayerOneAttack] && !keys[controls.PlayerOneBlock]) {
          if (!keys[controls.PlayerTwoBlock]) {
            console.log(`${firstFighter.name} is attacking,  ${secondFighter.name} is not blocking`);
            updateHealth(firstFighter, secondFighter, secondFighterBar, getDamage, initHealth2)
            move(firstAttacker, damageColor);
          } else if (keys[controls.PlayerTwoBlock]) {
            console.log(`${secondFighter.name} is blocking, no damage inflicted`)
            move(firstAttacker, blockColor);
          }
        } else if (keys[controls.PlayerTwoAttack] && !keys[controls.PlayerTwoBlock]) {
          if (!keys[controls.PlayerOneBlock]) {
            console.log(`${firstFighter.name} is attacking,  ${secondFighter.name} is not blocking`);
            updateHealth(secondFighter, firstFighter, firstFighterBar, getDamage, initHealth1)
            move(secondAttacker, damageColor)
          } else if (keys[controls.PlayerOneBlock]) {
            console.log(`${secondFighter.name} is blocking, no damage inflicted`)
            move(secondAttacker, blockColor)
          }
        }
        defineWinner(firstFighter, secondFighter);
      }

      if (winner) {
        resolve(winner);
      }
    }

    // change the position of the attacker/defender
    function move(fighter, color) {
      if (fighter == "firstFighter") {
        arena.style.justifyContent = "flex-end";
        secondAttackerImgbox.style.background = color;
        setTimeout(() => {
          secondAttackerImgbox.style.background = "none";
        }, 300);
      }
      if (fighter == "secondFighter") {
        arena.style.justifyContent = "flex-start";
        firstAttackerImgbox.style.background = color;
        setTimeout(() => {
          firstAttackerImgbox.style.background = "none";
        }, 300);
      }
    }


    // define the winner based on health left and tweek the health bar
    function defineWinner(firstFighter, secondFighter) {
      if (firstFighter.health <= 0 || secondFighter.health <= 0) {
        if (firstFighter.health <= 0) {
          firstFighter.health = 0;
          winner = secondFighter;
          firstFighterBar.style.width = "0";
        } else if (secondFighter.health <= 0) {
          secondFighter.health = 0;
          winner = firstFighter;
          secondFighterBar.style.width = "0";
        }
      }
    }


    // Block super attack for 10 seconds
    function delay(fighterToBlock) {
      if (fighterToBlock == "firstFighter") {
        blockFighterOne = true;
        console.log('10 sec block after super attack');
        setTimeout(() => {
          blockFighterOne = false;
        }, 10000)
      } else if (fighterToBlock == "secondFighter") {
        blockFighterTwo = true;
        console.log('10 sec block after super attack');
        setTimeout(() => {
          blockFighterTwo = false;
        }, 10000);
      }
    }

    // empty the object with stored key presses
    function keysReleased(e) {
      keys[e.code] = false;
    }
  })
}



function updateHealth(attacker, defender, bar, damageFunction, initHealth) {

  const damage = damageFunction(attacker, defender);
  defender.health -= damage;
  const damagePercent = Math.floor((defender.health / initHealth) * 100);
  bar.style.width = `${damagePercent}%`;
  return defender.health;
}

// calculates damage from 3 possible attacks: simple, simple + opponent is blocking, super.
export function getDamage(attacker, defender) {
  let damage;
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender)
  if (blockPower > hitPower) {
    damage = 0;
    console.log('Simple attack -> no damage, cos of good block');
  } else {
    damage = hitPower - blockPower;
    console.log('Simple attack -> some block');
  }

  return damage;
}


function superAttack(attacker) {
  const damage = 2 * getHitPower(attacker);
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  console.log(`default power: ${fighter.attack}, chance: ${criticalHitChance}, hit power: ${power}`);
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const block = fighter.defense * dodgeChance;
  console.log(`defense: ${fighter.defense}, dodgeChance: ${dodgeChance}, block: ${block}`);
  return block;
}