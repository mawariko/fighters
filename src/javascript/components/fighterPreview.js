import {
  createElement
} from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const infoBox = createElement({
    tagName: 'div',
    className: 'fighter-preview__infobox',
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    for (let key in fighter) {
      if (key !== "_id" && key !== "source") {
        const infoP = createElement({
          tagName: 'p',
          className: 'fighter-preview__text'
        });
        infoP.textContent = `${key}: ${fighter[key]}`;
        infoBox.appendChild(infoP)
      } else if (key == "source") {
        const fighterImage = createFighterImage(fighter);
        fighterImage.classList.add('fighter-preview__img')
        fighterElement.appendChild(fighterImage);
      }
    }
  }


  fighterElement.appendChild(infoBox);
  return fighterElement;
}

  export function createFighterImage(fighter) {
    const {
      source,
      name
    } = fighter;
    const attributes = {
      src: source,
      title: name,
      alt: name
    };
    const imgElement = createElement({
      tagName: 'img',
      className: '',
      attributes
    });

    return imgElement;
  }